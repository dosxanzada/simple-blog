exports.config = {
    env : 'development', //production
    port : 3333,
    encoding : 'utf8',
    siteinfo : {
        lang : 'en-US',
        name : "Блог PolyTech",
        desc : "Веб-сайт для введения блога для студентов Satbayev University",
        keywords : "blog,node.js,javascript,express,jade,html5",
        logo : '/images/logo.jpg',
        comment_service: "",
        comment_id : "",
        gaid : "",
        footer : 'Блог PolyTech © 2019 <a link="https://satbayev.university/">Satbayev University',
    	theme : "style-github",
    	hljs_host : "local",
    	hljs_theme : "github",
    	jquery_host : "local"
    },
    comments : {
        'duoshuo' : 'duoshuo.com',
        'disqus' : 'disqus.com',
        'livefyre' : 'livefyre.com'
    },
    salt : 'xew24igjs',
    user : {
        cookie : 'token',
        username : 'admin',
        password : 'e10adc3949ba59abbe56e057f20f883e' //Current is 123456, please remember to change this password!!!
    },
    user1 : {
        cookie : 'token',
        username : 'dosxanzada',
        password : 'e10adc3949ba59abbe56e057f20f883e' //Current is 123456, please remember to change this password!!!
    },
    webinfo : {},
    links : {}
};
